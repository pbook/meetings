﻿using Meetings.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Meetings.Data.Repository;

namespace Meetings.Web.Controllers
{
    public class MeetingsController : ApiController
    {
        public IHttpActionResult Get()
        {
            List<Meeting> meetings = new MeetingRepository().GetAll();

            return Ok(meetings);
        }

        public IHttpActionResult Get(int id)
        {
            Meeting meeting = new MeetingRepository().Get(id);
            if (meeting == null)
            {
                return NotFound();
            }

            return Ok(meeting);
        }

        public IHttpActionResult Post(Meeting meeting)
        {
            MeetingRepository repository = new MeetingRepository();
            repository.Insert(meeting);

            return Ok(meeting);
        }

        public IHttpActionResult Put(Meeting meeting)
        {
            MeetingRepository repository = new MeetingRepository();
            repository.Update(meeting);

            return Ok(meeting);
        }

        public IHttpActionResult Delete(int id)
        {
            MeetingRepository repository = new MeetingRepository();
            Meeting meeting = repository.Get(id);
            if (meeting == null)
            {
                return NotFound();
            }

            repository.Delete(meeting);

            return Ok();
        }
    }
}
