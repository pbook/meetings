﻿using Meetings.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meetings.Data.Repository
{
    public class MeetingRepository
    {
        private MeetingsContext db;

        public MeetingRepository()
        {
            db = new MeetingsContext();
        }

        public List<Meeting> GetAll()
        {
            return db.Meetings.ToList();
        }

        public Meeting Get(int id)
        {
            return db.Meetings.Find(id);
        }

        public void Insert(Meeting meeting)
        {
            db.Meetings.Add(meeting);
            db.SaveChanges();
        }

        public void Update(Meeting meeting)
        {
            db.Entry(meeting).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(Meeting meeting)
        {
            db.Meetings.Remove(meeting);
            db.SaveChanges();
        }
    }
}
