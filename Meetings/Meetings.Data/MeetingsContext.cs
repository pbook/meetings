﻿using Meetings.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meetings.Data
{
    class MeetingsContext : DbContext
    {
        public MeetingsContext() : base("MeetingsConnection")
        {

            var ensureDLLIsCopied =
                    System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public DbSet<Meeting> Meetings { get; set; }
    }
}
